package edu.dhbw.mvvmexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import edu.dhbw.mvvmexample.ui.main.ContactFragment

/**
 * A simple Activity hosting a ContactFragment.kt.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, ContactFragment.newInstance())
                    .commitNow()
        }
    }
}