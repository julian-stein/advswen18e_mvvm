package edu.dhbw.mvvmexample.datalayer

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import edu.dhbw.mvvmexample.model.Contact
import edu.dhbw.mvvmexample.model.ContactDao

@Database(entities = [Contact::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class ContactDatabase : RoomDatabase() {
    abstract val contactDao: ContactDao

    companion object {
        @Volatile
        private var INSTANCE: ContactDatabase? = null
        fun getInstance(context: Context): ContactDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                            context.applicationContext,
                            ContactDatabase::class.java,
                            "contact_database.db"
                    )
                            .createFromAsset("database/contact_database.db")
                            .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}