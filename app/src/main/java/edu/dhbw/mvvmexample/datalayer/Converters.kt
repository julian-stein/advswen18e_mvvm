package edu.dhbw.mvvmexample.datalayer

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import edu.dhbw.mvvmexample.enums.PhoneNumberType
import java.time.LocalDate

/**
 * A class consisting of TypeConverter functions used to persist instances of the Contact entity
 * in the ContactDatabase.
 */
class Converters {
    @TypeConverter
    fun fromEpochDay(value: Long): LocalDate {
        return LocalDate.ofEpochDay(value)
    }

    @TypeConverter
    fun localDateToEpochDay(localDate: LocalDate): Long {
        return localDate.toEpochDay()
    }

    @TypeConverter
    fun fromStringPhoneNumberType(value: String): PhoneNumberType {
        return PhoneNumberType.getByValue(value)
    }

    @TypeConverter
    fun phoneNumberTypeToString(phoneNumberType: PhoneNumberType): String {
        return phoneNumberType.value
    }

    @TypeConverter
    fun phoneNumbersMapToString(phoneNumbers: Map<PhoneNumberType, String>): String {
        return Gson().toJson(phoneNumbers)
    }

    @TypeConverter
    fun stringToPhoneNumbersMap(value: String): Map<PhoneNumberType, String> {
        return Gson().fromJson(value, object : TypeToken<Map<PhoneNumberType, String>>() {}.type)
    }
}