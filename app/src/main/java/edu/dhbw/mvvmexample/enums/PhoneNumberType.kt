package edu.dhbw.mvvmexample.enums

/**
 * A simple enum representing the different types of phone numbers.
 * Offers a method to parse a value from a given String.
 */
enum class PhoneNumberType(val value: String) {
    MOBILE("Mobil"),
    PRIVATE("Privat"),
    WORK("Arbeit");

    companion object {
        private val VALUES = values()

        /**
         * Parse the given String to the corresponding enum value.
         */
        fun getByValue(value: String) = VALUES.first { it.value == value }
    }
}