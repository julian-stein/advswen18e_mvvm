package edu.dhbw.mvvmexample.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import edu.dhbw.mvvmexample.R
import edu.dhbw.mvvmexample.enums.PhoneNumberType
import java.time.LocalDate

/**
 * A simple data class consisting of relevant contact info.
 */
@Entity
data class Contact(
        @PrimaryKey(autoGenerate = true)
        var contactId: Long,
        var firstName: String,
        var lastName: String,
        var pictureRef: Int,
        var phoneNumbers: Map<PhoneNumberType, String>,
        var streetName: String,
        var number: Int,
        var city: String,
        var favourite: Boolean
) {
    companion object {
        /**
         * Get a demo instance of Contact.
         */
        fun demoContact(): Contact {
            return Contact(
                    1L,
                    "Aylin",
                    "Al-Hamadi",
                    R.drawable.stock_contact,
                    mapOf(
                            PhoneNumberType.MOBILE to "+49 123 456789",
                            PhoneNumberType.PRIVATE to "+49 987 654321",
                            PhoneNumberType.WORK to "+49 711 123456"
                    ),
                    "Königsstraße",
                    42,
                    "Stuttgart",
                    false
            )
        }
    }
}