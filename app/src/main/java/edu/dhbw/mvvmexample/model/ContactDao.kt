package edu.dhbw.mvvmexample.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

/**
 * An interface declaring database access functions for the ContactDatabase.
 */
@Dao
interface ContactDao {
    @Insert
    fun insert(contact: Contact)

    @Update
    fun update(contact: Contact)

    @Query("select * from contact where contactId = :id")
    fun getById(id: Long): LiveData<Contact>
}
