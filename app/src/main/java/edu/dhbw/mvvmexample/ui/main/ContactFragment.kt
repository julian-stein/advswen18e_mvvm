package edu.dhbw.mvvmexample.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import edu.dhbw.mvvmexample.R
import edu.dhbw.mvvmexample.databinding.FragmentContactBinding
import edu.dhbw.mvvmexample.datalayer.ContactDatabase
import edu.dhbw.mvvmexample.enums.PhoneNumberType

/**
 * A simple Fragment subclass displaying an image and basic information of a phone contact.
 */
class ContactFragment : Fragment() {

    companion object {
        fun newInstance() = ContactFragment()
    }

    private lateinit var viewModel: ContactViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding: FragmentContactBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_contact, container, false)

        val application = requireNotNull(this.activity).application
        val dao = ContactDatabase.getInstance(application).contactDao

        viewModel = ViewModelProvider(
                this,
                ContactViewModelFactory(dao, application)
        ).get(ContactViewModel::class.java)

        binding.contactViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        // Set up the dropdown menu with selected value and options.
        (binding.phoneNumberTypeSelector.editText as MaterialAutoCompleteTextView)
                .setText(viewModel.phoneNumberType.value?.value as CharSequence, false)
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item_dropdown, PhoneNumberType.values().map { it.value })
        (binding.phoneNumberTypeSelector.editText as MaterialAutoCompleteTextView).setAdapter(adapter)

        return binding.root
    }

}