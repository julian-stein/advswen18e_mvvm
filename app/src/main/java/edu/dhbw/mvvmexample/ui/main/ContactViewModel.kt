package edu.dhbw.mvvmexample.ui.main

import android.app.Application
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import edu.dhbw.mvvmexample.enums.PhoneNumberType
import edu.dhbw.mvvmexample.model.Contact
import edu.dhbw.mvvmexample.model.ContactDao
import kotlinx.coroutines.*

/**
 * A custom ViewModel subclass providing LiveData data sources and handler functions for user input
 * for the ContactFragment.
 */
class ContactViewModel(
        private val dao: ContactDao,
        private val application: Application
) : ViewModel() {

    private var viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val contact: LiveData<Contact> = dao.getById(1L)

    val displayableContact: LiveData<DisplayableContact> =
            Transformations.map(contact) {
                DisplayableContact.fromContact(it, application)
            }

    private val _phoneNumberType: MutableLiveData<PhoneNumberType> = MutableLiveData(PhoneNumberType.MOBILE)
    val phoneNumberType: LiveData<PhoneNumberType>
        get() = _phoneNumberType

    /**
     * Update this._phoneNumberType LiveData by parsing the given CharSequence to a PhoneNumberType
     * enum value.
     * @param value the CharSequence to parse the enum value from.
     */
    fun onPhoneNumberTypeChanged(value: CharSequence) {
        _phoneNumberType.value = PhoneNumberType.getByValue(value.toString())
    }

    /**
     * Toggle this.contact's favourite boolean value and initiate an update in the database.
     */
    fun onFavouriteCheckedChanged() {
        contact.value?.let { currentContact ->
            currentContact.favourite = !currentContact.favourite
            uiScope.launch {
                updateContactInDb(currentContact)
            }
        }
    }

    /**
     * Initiate an update of this.contact in the database via the model.
     * @param toUpdate the contact to update with new value(s).
     */
    private suspend fun updateContactInDb(toUpdate: Contact) {
        withContext(Dispatchers.IO) {
            dao.update(toUpdate)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    /**
     * A simple data class used as a data source for the ContactFragment's UI.
     */
    data class DisplayableContact(
            var name: String,
            var picture: Drawable,
            var phoneNumbers: Map<PhoneNumberType, String>,
            var address: String,
            var city: String,
            var favourite: Boolean
    ) {
        companion object {
            /**
             * Create a DisplayableContact object from a given Contact object copying UI-relevant
             * parameters, concatenating parameters to be displayed as one string and creating
             * a Drawable from the resource id of the Contact object.
             * @param contact the contact to transform into a displayable contact.
             * @param application the application; used to load the contact image.
             */
            fun fromContact(contact: Contact, application: Application) =
                    DisplayableContact(contact.firstName + " " + contact.lastName,
                            ContextCompat.getDrawable(application, contact.pictureRef)!!,
                            contact.phoneNumbers,
                            contact.streetName + " " + contact.number,
                            contact.city,
                            contact.favourite
                    )
        }
    }
}