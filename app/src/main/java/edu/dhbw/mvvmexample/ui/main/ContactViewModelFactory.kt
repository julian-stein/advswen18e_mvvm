package edu.dhbw.mvvmexample.ui.main

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import edu.dhbw.mvvmexample.model.ContactDao

/**
 * Custom Factory implementation for the ContactViewModel taking a ContactDao and Application
 * instance as parameters to create the ContactViewModel with.
 */
class ContactViewModelFactory(
        private val dao: ContactDao,
        private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ContactViewModel::class.java)) {
            return ContactViewModel(dao, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}